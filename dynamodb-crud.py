import json
import boto3

dynamodb = boto3.client('dynamodb')
table_name = 'users'  # Replace with your table name

def lambda_handler(event, context):
    # Check if the input data is present in the event
    if 'data' not in event:
        return {
            'statusCode': 400,
            'body': json.dumps('Error: Input data is missing')
        }

    # Extract data from the input
    data_to_insert = event['data']

    try:
        # Insert data into the DynamoDB table
        response = dynamodb.put_item(
            TableName=table_name,
            Item={
                'id': {'S': data_to_insert.get('id', '')},
                'name': {'S': data_to_insert.get('name', '')},
                'city': {'S': data_to_insert.get('city', '')}
            }
        )

        # Check if the operation was successful
        if response.get('ResponseMetadata', {}).get('HTTPStatusCode') == 200:
            return {
                'statusCode': 200,
                'body': json.dumps('Data inserted successfully')
            }
        else:
            return {
                'statusCode': 500,
                'body': json.dumps('Error: Unexpected response from DynamoDB')
            }
    except Exception as e:
        # Log the error for troubleshooting
        print(f'Error: {str(e)}')
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error: {str(e)}')
        }