import json
import boto3

def lambda_handler(event, context):
    # Check if 'bucket_name' is provided in the request body
    if 'body' in event and event['body']:
        body = event['body']
        try:
            # Parse the JSON body
            input_data = json.loads(body)
            if 'bucket_name' in input_data:
                bucket_name = input_data['bucket_name']

                # Create an S3 client
                s3 = boto3.client('s3')

                # Create S3 bucket
                s3.create_bucket(Bucket=bucket_name, CreateBucketConfiguration={'LocationConstraint': 'ap-south-1'})

                message = f"S3 bucket '{bucket_name}' created successfully."
            else:
                message = "Please provide a 'bucket_name' in the request body."
        except json.JSONDecodeError:
            message = "Invalid JSON format in the request body."
    else:
        message = "Please provide a request body with a 'bucket_name'."

    return {
        'statusCode': 200,
        'body': message
    }