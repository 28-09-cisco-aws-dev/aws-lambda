import json

def lambda_handler(event, context):
    # Check if 'name' is provided in the request body
    if 'body' in event and event['body']:
        body = event['body']
        try:
            # Parse the JSON body
            input_data = json.loads(body)
            if 'name' in input_data:
                name = input_data['name']
                message = f"Hello, {name}!"
            else:
                message = "Hello, World!"
        except json.JSONDecodeError:
            message = "Invalid JSON format in the request body."
    else:
        message = "Hello, World!"

    return {
        'statusCode': 200,
        'body': message
    }